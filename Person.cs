using System;

namespace task28
{

class Person
    {
        //public Person(){} --was a  hack to get around the : base from the subclass
        private string Name;
        private int Age;

        public Person (string _name, int _age)
        {
            Name = _name;
            Age = _age;
        }
        public void SayHello()
        {
            Console.WriteLine($"Ahoy! My name is {Name} and I am {Age} years old");
        }
    }
}